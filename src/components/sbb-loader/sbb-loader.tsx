import { Component, Prop } from '@stencil/core';


@Component({
	tag: 'sbb-loader',
	styleUrl: 'sbb-loader.scss'
})
export class SbbLoader {

	@Prop() show: boolean;

	render() {
		if (!this.show) {
			return null;
		}

		return(
			<div class='spinner'>
				<div class='double-bounce1'></div>
				<div class='double-bounce2'></div>
			</div>
		);
	}
}
