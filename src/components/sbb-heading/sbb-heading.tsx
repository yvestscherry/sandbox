import { Component, Prop } from '@stencil/core';

@Component({
	tag: 'sbb-heading',
	styleUrl: 'sbb-heading.scss',
	shadow: true
})
export class SbbHeading {

	@Prop() text: string;
	@Prop() level: number;

	render() {
		return (
			 h(
				 `h${this.level}`,
				 { className: 'heading'},
				 this.text
			 )
		);
	};
}
