import { Component } from '@stencil/core';


@Component({
	tag: 'sbb-definition-definition',
	styleUrl: 'sbb-definition-definition.scss',
	shadow: true
})
export class SbbDefinitionDefinition {

	render() {
		return (
			<dd>
				<slot />
			</dd>
		);
	}
}
