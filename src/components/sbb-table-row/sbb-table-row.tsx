import { Component, Element } from '@stencil/core';


@Component({
	tag: 'sbb-table-row',
	styleUrl: 'sbb-table-row.scss'
})
export class SbbTableRow {

	@Element() tr: HTMLTableElement;

	componentDidLoad() {
		this.tr.setAttribute('role', 'row');
		this.tr.style.display = 'table-row';
	}

	render() {
		return (
			<slot />
		);
	}
}
