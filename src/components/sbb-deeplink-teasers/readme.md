# sbb-deeplink-teasers



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type     | Default     |
| ------------- | -------------- | ----------- | -------- | ----------- |
| `items`       | `items`        |             | `string` | `undefined` |
| `titlesLevel` | `titles-level` |             | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
