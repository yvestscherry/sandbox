import { Component, Prop } from '@stencil/core';
import imageFile from './sbb-homepage-main-teaser-image-file';

@Component({
	tag: 'sbb-homepage-main-teaser-image',
	styleUrl: 'sbb-homepage-main-teaser-image.scss',
	shadow: true
})

export class SbbHomepageMainTeaserImage {

	@Prop() image: string;
	render() {
		const imageSrc = this.image ? this.image : `data:image/png;base64,${imageFile}`;
		return (
			<div class='image-wrapper-outer'>
				<svg class='clip-top' viewBox="0 0 100 100" preserveAspectRatio="none slice" width="100%">
					<path d="M 0 0 L 0 100 L 100 5 L 100 0 L 0 0 Z"></path>
				</svg>
				<svg class='clip-bottom' viewBox="0 0 100 100" preserveAspectRatio="none slice" width="100%">
					<path d="M 0 0 L 0 100 L 101 100 L 0 0 Z"></path>
				</svg>
				<div class='image-wrapper'>
					<img class='image' src={imageSrc} />
				</div>
			</div>
		);
	}
}
