import { Component, Prop } from '@stencil/core';


@Component({
	tag: 'sbb-input-text',
	styleUrl: 'sbb-input-text.scss',
	shadow: true
})
export class SbbInputText {

	@Prop() label: string;
	@Prop() name: string;
	@Prop() id: string;

	render() {
		return (
			<div>
				<label>
					Enter your name:
					<input type="text" name={this.name} id={this.id} required />
				</label>
			</div>
		);
	}
}
