import { Component, Prop } from '@stencil/core';

@Component({
	tag: 'sbb-homepage-main-teaser',
	styleUrl: 'sbb-homepage-main-teaser.scss',
	shadow: true
})

export class SbbHomepageMainTeaser {
	@Prop() image?: string;

	render() {
		return (
			<a href='#' class='mainteaser'>
				<sbb-homepage-main-teaser-image image={this.image}></sbb-homepage-main-teaser-image>
				<sbb-red-trapeze></sbb-red-trapeze>
				<sbb-white-trapeze></sbb-white-trapeze>
			</a>
		);
	}
}
