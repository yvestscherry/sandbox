import { Component} from '@stencil/core';


@Component({
	tag: 'sbb-list',
	styleUrl: 'sbb-list.scss',
	// shadow: true
})
export class SbbList {
	render() {
		return (
			<ul class='list'>
				<slot />
			</ul>
		);
	}
}
