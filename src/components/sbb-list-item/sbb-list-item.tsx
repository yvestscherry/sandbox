import { Component } from '@stencil/core';


@Component({
	tag: 'sbb-list-item',
	styleUrl: 'sbb-list-item.scss',
	shadow: true
})
export class SbbListItem {

	render() {
		return (
			<li>
				<slot />
			</li>
		);
	}
}
