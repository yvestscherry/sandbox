import { Component, Prop } from '@stencil/core';


@Component({
	tag: 'sbb-button',
	styleUrl: 'sbb-button.scss',
	shadow: true
})
export class SbbButton {

	@Prop() type: string;
	@Prop() text: string;

	render() {
		return (
			<input type={this.type} value={this.text} />
		);
	}
}
