import { Component, Prop } from '@stencil/core'


@Component({
	tag: 'sbb-timetable-result',
	styleUrl: 'sbb-timetable-result.scss',
	shadow: true
})
export class SbbTimetableResult {

	@Prop() originLocation: string
	@Prop() originTime: string
	@Prop() originTrack: string
	@Prop() destinationLocation: string
	@Prop() destinationTime: string
	@Prop() destinationTrack: string

	// private buy = (): void => {
	// 	const data = {
	// 		originLocation: this.originLocation,
	// 		originTime: this.originTime,
	// 		originTrack: this.originTrack,
	// 		locationdestination: this.destinationLocation,
	// 		timedestination: this.destinationTime,
	// 		trackdestination: this.destinationTrack,
	// 	}
	// }

	render() {
		function formatTime (dateString) {
			const date = new Date(dateString)
			const hours = date.getHours()
			const hoursPadding = hours < 10 ? '0' : ''
			const minutes = date.getMinutes()
			const minutesPadding = minutes < 10 ? '0' : ''
			return hoursPadding + hours + ':' + minutesPadding + minutes
		}

		return (
			<li class='result'>
				<div class='origin'>
					<span class='location'>{this.originLocation}</span>
					<span class='time'>{formatTime(this.originTime)}</span>
					<span class='track'>Track {this.originTrack}</span>
				</div>

				<div class='destination'>
					<span class='location'>{this.destinationLocation}</span>
					<span class='time'>{formatTime(this.destinationTime)}</span>
					<span class='track'>Track {this.destinationTrack}</span>
				</div>
				<sbb-cta-button>Kaufen</sbb-cta-button>
			</li>
		);
	}
}
