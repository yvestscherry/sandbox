# sbb-timetable-result



<!-- Auto Generated Below -->


## Properties

| Property              | Attribute              | Description | Type     | Default     |
| --------------------- | ---------------------- | ----------- | -------- | ----------- |
| `destinationLocation` | `destination-location` |             | `string` | `undefined` |
| `destinationTime`     | `destination-time`     |             | `string` | `undefined` |
| `destinationTrack`    | `destination-track`    |             | `string` | `undefined` |
| `originLocation`      | `origin-location`      |             | `string` | `undefined` |
| `originTime`          | `origin-time`          |             | `string` | `undefined` |
| `originTrack`         | `origin-track`         |             | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
