import { Component } from '@stencil/core';


@Component({
	tag: 'sbb-definition-list',
	styleUrl: 'sbb-definition-list.scss',
	shadow: true
})
export class SbbDefinitionList {

	render() {
		return (
			<dl>
				<slot />
			</dl>
		);
	}
}
