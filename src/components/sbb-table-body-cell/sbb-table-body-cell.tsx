import { Component, Element } from '@stencil/core';


@Component({
	tag: 'sbb-table-body-cell',
	styleUrl: 'sbb-table-body-cell.scss'
})
export class SbbTableBodyCell {

	@Element() td: HTMLTableElement;

	componentDidLoad() {
		this.td.setAttribute('role', 'cell');
		this.td.style.display = 'table-cell';
	}

	render() {
		return (
			<slot />
		);
	}
}
