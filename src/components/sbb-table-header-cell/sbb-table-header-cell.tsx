import { Component, Element } from '@stencil/core';


@Component({
	tag: 'sbb-table-header-cell',
	styleUrl: 'sbb-table-header-cell.scss'
})
export class SbbTableHeaderCell {

	@Element() th: HTMLTableElement;

	componentDidLoad() {
		this.th.setAttribute('role', 'columnheader');
		this.th.style.display = 'table-cell';
	}

	render() {
		return (
			<slot />
		);
	}
}
