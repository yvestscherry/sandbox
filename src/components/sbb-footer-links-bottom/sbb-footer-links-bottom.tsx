import { Component } from '@stencil/core';
import swisspassIcon from './sbb-footer-links-bottom-swisspass';

@Component({
	tag: 'sbb-footer-links-bottom',
	styleUrl: 'sbb-footer-links-bottom.scss',
	shadow: true
})

export class SbbFooterLinksBottom {

	render() {
		return (
			<sbb-list>
				<sbb-list-link-item link='#' linktext='Impressum'></sbb-list-link-item>
				<sbb-list-link-item link='#' linktext='Rechtlicher Hinweis'></sbb-list-link-item>
				<sbb-list-link-item link='#' linktext='Datenschutz'></sbb-list-link-item>
				<div class='icon' innerHTML={swisspassIcon}></div>
			</sbb-list>
		);
	}
}
