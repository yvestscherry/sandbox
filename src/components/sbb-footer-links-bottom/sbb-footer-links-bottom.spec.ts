import { TestWindow } from '@stencil/core/testing';
import { SbbFooterLinksBottom } from './sbb-footer-links-bottom';

describe('sbb-footer-links-bottom', () => {
  it('should build', () => {
    expect(new SbbFooterLinksBottom()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLSbbFooterLinksBottomElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [SbbFooterLinksBottom],
        html: '<sbb-footer-links-bottom>' 
          + '</sbb-footer-links-bottom>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
