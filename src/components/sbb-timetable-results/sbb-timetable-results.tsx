import { Component } from '@stencil/core';

@Component({
	tag: 'sbb-timetable-results',
	styleUrl: 'sbb-timetable-results.scss',
	shadow: true
})

export class SbbTimetableResults {

	render() {

		return (
			<ul class='results'>
				<slot></slot>
			</ul>
		)
	}
}
