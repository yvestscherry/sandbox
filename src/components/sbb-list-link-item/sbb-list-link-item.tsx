import { Component, Prop } from '@stencil/core';


@Component({
	tag: 'sbb-list-link-item',
	styleUrl: 'sbb-list-link-item.scss',
	// shadow: true
})
export class SbbListLinkItem {
	@Prop() link: string;
	@Prop() linktext: string;

	render() {
		return (
			<li class='item'>
				<a href={this.link} class='link'>{this.linktext}</a>
			</li>
		);
	}
}
