import { Component, State } from '@stencil/core';

@Component({
	tag: 'sbb-button-expand',
	styleUrl: 'sbb-button-expand.scss'
})
export class SbbButtonExpand {

	@State() expanded: boolean = false;
	buttonState!: HTMLElement;
	button!: HTMLElement;

	setButtonState = () => {
		this.buttonState.textContent = this.expanded ? 'expanded' : 'collapsed'
	};

	buttonClick = () => {
		this.expanded = !this.expanded;
		this.setButtonState();
	};

	render() {
		return (
			<button ref={(el) => this.button = el as HTMLElement} onClick={ () => this.buttonClick()} aria-expanded={this.expanded ? 'true' : 'false'}>
				Current state: <span ref={(el) => this.buttonState = el as HTMLElement}>collapsed</span>
			</button>
		);
	}
}
