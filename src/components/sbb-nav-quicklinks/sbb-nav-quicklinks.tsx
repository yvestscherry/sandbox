import { Component } from '@stencil/core';

@Component({
	tag: 'sbb-nav-quicklinks',
	styleUrl: 'sbb-nav-quicklinks.scss',
	shadow: true
})

export class SbbNavQuicklinks {
	render() {
		return (
			<sbb-list>
				<sbb-list-link-item link='#' linktext='SBB News'></sbb-list-link-item>
				<sbb-list-link-item link='#' linktext='SBB Community'></sbb-list-link-item>
				<sbb-list-link-item link='#' linktext='Jobs'></sbb-list-link-item>
			</sbb-list>
		);
	}
}
