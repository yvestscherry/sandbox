import { Component, Prop } from '@stencil/core';


@Component({
	tag: 'sbb-cta-button',
	styleUrl: 'sbb-cta-button.scss',
	shadow: true
})
export class SbbCtaButton {

	@Prop() clickCallback: () => void;

	handleClick() {
		if (this.clickCallback) {
			this.clickCallback();
		}
	}

	render() {
		return (
			<button
				class='button'
				onClick={this.handleClick.bind(this)}
			>
				<slot />
			</button>
		);
	}
}
