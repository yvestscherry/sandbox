import { TestWindow } from '@stencil/core/testing';
import { SbbPagetitle } from './sbb-pagetitle';

describe('sbb-pagetitle', () => {
  it('should build', () => {
    expect(new SbbPagetitle()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLSbbPagetitleElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [SbbPagetitle],
        html: '<sbb-pagetitle>' 
          + '</sbb-pagetitle>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
