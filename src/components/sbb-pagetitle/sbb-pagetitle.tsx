import {Component, Prop} from '@stencil/core';


@Component({
	tag: 'sbb-pagetitle',
	styleUrl: 'sbb-pagetitle.scss',
	shadow: true
})

export class SbbPagetitle {

	@Prop() pageTitle: string;
	@Prop() additionalClasses: string;
	@Prop() visuallyhidden: string;

	render() {

		let additionalClasses = this.additionalClasses || '';

		if (this.visuallyhidden === 'true') {
			additionalClasses += ' visuallyhidden'
		}

		return (
			<h1 class={additionalClasses}>
				{this.pageTitle}
			</h1>
		);
	}
}
