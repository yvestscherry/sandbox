import { TestWindow } from '@stencil/core/testing';
import { SbbFooterColumnRte } from './sbb-footer-column-rte';

describe('sbb-footer-column-rte', () => {
  it('should build', () => {
    expect(new SbbFooterColumnRte()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLSbbFooterColumnRteElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [SbbFooterColumnRte],
        html: '<sbb-footer-column-rte>' 
          + '</sbb-footer-column-rte>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
