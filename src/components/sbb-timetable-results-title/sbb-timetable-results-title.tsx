import { Component, Prop } from '@stencil/core';
import icon from './sbb-timetable-results-title-icon';

@Component({
	tag: 'sbb-timetable-results-title',
	styleUrl: 'sbb-timetable-results-title.scss',
	shadow: true
})
export class SbbTimetableResultsTitle {

	@Prop() from: string;
	@Prop() to: string;

	render() {

		if (!this.from || !this.to) {
			return null;
		}

		return (
			<h2 class='title'>
				<span class='from'>{this.from}</span>
				<span class='icon' innerHTML={icon}></span>
				<span class='to'>{this.to}</span>
			</h2>
		);
	}
}
