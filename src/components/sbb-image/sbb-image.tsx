import { Component, Prop } from '@stencil/core';


@Component({
	tag: 'sbb-image',
	styleUrl: 'sbb-image.scss',
	shadow: true
})
export class SbbImage {

	@Prop() src: string;
	@Prop() alt: string;

	render() {
		return (
			<img src={this.src} alt={this.alt} />
		);
	}
}
