import { Component } from '@stencil/core';


@Component({
	tag: 'sbb-table-caption',
	styleUrl: 'sbb-table-caption.scss'
})
export class SbbTableCaption {
	render() {
		return (
			<div id='table'>
				<slot />
			</div>
		);
	}
}
