import {Component, Prop} from '@stencil/core';
import icon from './sbb-footer-column-title-icon';

@Component({
	tag: 'sbb-footer-column-title',
	styleUrl: 'sbb-footer-column-title.scss',
	shadow: true
})

export class SbbFooterColumnTitle {

	@Prop() columntitle: string;

	render() {
		return (
			<h2>
				{this.columntitle}

				<span class='icon'>
					<span class='icon-close' innerHTML={icon}></span>
					<span class='icon-open' innerHTML={icon}></span>
				</span>
			</h2>
		);
	}
}
