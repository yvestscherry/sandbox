import { TestWindow } from '@stencil/core/testing';
import { SbbFooterColumnTitle } from './sbb-footer-column-title';

describe('sbb-footer-column-title', () => {
  it('should build', () => {
    expect(new SbbFooterColumnTitle()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLSbbFooterColumnTitleElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [SbbFooterColumnTitle],
        html: '<sbb-footer-column-title>'
          + '</sbb-footer-column-title>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
