import { Component, Prop, State } from '@stencil/core';

import sbbLogo from './sbb-header-logo';
import toggleIcon from './sbb-nav-toggle';
import closeIcon from './sbb-nav-close';

@Component({
	tag: 'sbb-header',
	styleUrl: 'sbb-header.scss',
	shadow: true
})

export class SbbHeader {

	@Prop() items: string;
	@Prop() language: string;
	@State() navOpen: boolean = false;

	toggleNavigation() {
		this.navOpen = !this.navOpen;
	}

	render() {

		const itemsObject = JSON.parse(this.items);
		const itemsForCurrentLanguage = itemsObject[this.language];
		const toggledClass = (this.navOpen ? ' nav--toggled' : '');
		const listClass = 'nav' + toggledClass;
		const menuIcon = this.navOpen ? closeIcon : toggleIcon;

		return (
			<div class='header'>
				<div class='header-wrapper'>
					<div
						class='toggle'
						onClick={this.toggleNavigation.bind(this)}
					>
						<span class='toggle__icon' innerHTML={menuIcon}></span>
					</div>
					<sbb-nav-quicklinks class={toggledClass}></sbb-nav-quicklinks>
					<div
						class='logo'
						innerHTML={sbbLogo}
					></div>
					<ul class={listClass}>
						{itemsForCurrentLanguage.map((item => {
							return (
								<li class='navitem'>
									<a
										class='navlink'
										href={item.link}
									>
										{item.title}
									</a>
								</li>
							);
						}))}
					</ul>
				</div>
			</div>
		);
	}
}
