import { Component, Prop } from '@stencil/core';


@Component({
	tag: 'sbb-link',
	styleUrl: 'sbb-link.scss',
	shadow: true
})
export class SbbLink {

	@Prop() href: string;
	@Prop() text: string;

	render() {
		return (
			<a href={this.href}>
				{this.text}
			</a>
		);
	}
}
