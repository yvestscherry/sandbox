# sbb-deeplink-teaser



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type                               | Default     |
| ------------- | -------------- | ----------- | ---------------------------------- | ----------- |
| `image`       | `image`        |             | `string`                           | `undefined` |
| `link`        | --             |             | `{ link: string; title: string; }` | `undefined` |
| `teaserTitle` | `teaser-title` |             | `string`                           | `undefined` |
| `text`        | `text`         |             | `string`                           | `undefined` |
| `titleLevel`  | `title-level`  |             | `number`                           | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
