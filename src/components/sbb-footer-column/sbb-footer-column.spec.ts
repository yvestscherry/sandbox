import { TestWindow } from '@stencil/core/testing';
import { SbbFooterColumn } from './sbb-footer-column';

describe('sbb-footer-column', () => {
  it('should build', () => {
    expect(new SbbFooterColumn()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLSbbFooterColumnElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [SbbFooterColumn],
        html: '<sbb-footer-column>' 
          + '</sbb-footer-column>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
