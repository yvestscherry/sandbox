import { Component, Element, State } from '@stencil/core';

@Component({
	tag: 'sbb-footer-column',
	styleUrl: 'sbb-footer-column.scss',
	shadow: true
})

export class SbbFooterColumn {
	@Element() private element: HTMLElement;
	@State() isOpen: boolean = false;
	@State() accordions: HTMLSbbFooterColumnElement[];

	titleElement: HTMLElement;

	componentDidLoad() {
		const allAccordions = document.querySelectorAll('sbb-footer-column');
		this.accordions = Array.from(allAccordions).filter(accordion => accordion !== this.element);

		this.element.addEventListener('transitionend', this.transitionEnd.bind(this));
		this.element.addEventListener('sbb-footer-column_close-accordion', this.closeAccordion.bind(this));
		this.titleElement = this.element.querySelector('sbb-footer-column-title');
	}

	componentDidUnload() {
		this.element.removeEventListener('transitionend', this.transitionEnd.bind(this));
		this.element.removeEventListener('sbb-footer-column_close-accordion', this.closeAccordion.bind(this));
	}

	transitionEnd() {
		this.element.style.height = null;
		if (this.isOpen) {
			this.element.classList.add('visible');
			this.titleElement.classList.add('visible');
		} else {
			this.element.classList.remove('visible');
			this.titleElement.classList.remove('visible');
		}
	}

	isDesktopViewport() {
		return window.innerWidth > 1024;
	}

	toggleAccordion() {
		if (this.isDesktopViewport()) {
			return;
		}

		if (!this.isOpen) {
			this.openAccordion();
		} else {
			this.closeAccordion();
		}
	}

	openAccordion() {
		this.isOpen = true;
		this.element.style.height = 'auto';
		const height = this.element.offsetHeight;
		this.element.style.height = '0px';

		setTimeout(() => {
			this.element.style.height = `${height}px`;
		}, 0);

		this.closeOpenAccordions();
	}

	closeAccordion() {
		if (!this.isOpen) {
			return;
		}

		this.isOpen = false;

		const height = this.element.offsetHeight;
		this.element.style.height = `${height}px`;

		setTimeout(() => {
			this.element.style.height = '0px';
		}, 0);
	}

	closeOpenAccordions() {
		if (!this.isOpen) {
			return;
		}

		const closeEvent = new CustomEvent('sbb-footer-column_close-accordion');

		this.accordions.forEach((accordion) => {
			accordion.dispatchEvent(closeEvent);
		});
	}

	render() {
		return [
			<li
				class="footer-columns-list-item"
				onClick={this.toggleAccordion.bind(this)}
			>
				<slot />
			</li>
		]
	}
}
