import { Component, Element } from '@stencil/core';


@Component({
	tag: 'sbb-table-head',
	styleUrl: 'sbb-table-head.scss'
})

export class SbbTableHead {

	@Element() thead: HTMLTableElement;

	componentDidLoad() {
		this.thead.setAttribute('role', 'rowgroup');
		this.thead.style.display = 'table-header-group';
	}

	render() {
		return (
			<slot />
		);
	}
}
