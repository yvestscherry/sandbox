import { Component } from '@stencil/core';

@Component({
	tag: 'sbb-white-trapeze',
	styleUrl: 'sbb-white-trapeze.scss',
	shadow: true
})

export class SbbWhiteTrapeze {
	render() {
		return (
			<div class='white-trapeze'>
				<span class='background'></span>
				<div class='content'>
					<span class="lead">Top-Hit</span>
					<span class="caption var_before">Bis zu</span>
					<span class="tag">
						<span class="prefix">–</span>50%
					</span>
				</div>
			</div>
		);
	}
}
