import { TestWindow } from '@stencil/core/testing';
import { SbbFooterColumns } from './sbb-footer-columns';

describe('sbb-footer-columns', () => {
  it('should build', () => {
    expect(new SbbFooterColumns()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLSbbFooterColumnsElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [SbbFooterColumns],
        html: '<sbb-footer-columns>' 
          + '</sbb-footer-columns>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
